<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?= dirname($_SERVER['PHP_SELF']) ?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= !empty($title) ? $title : 'title' ?></title>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>