<?php

/**
 * Home controller class responsible for handling requests to the homepage and contact page.
 * Extends the base Controller class to utilize common functionalities like rendering views.
 */
class Home extends Controller
{
    /**
     * Renders the homepage view.
     * 
     * Checks if a user is currently logged in and passes user information along with the page title
     * to the homepage view for rendering.
     */
    public function index()
    {
        $title = 'Hem'; // Page title
        $user = (new User)->logged_in(); // Check if user is logged in and get user details

        // Render the homepage view with title and user data
        $this->view('home/index', [
            'title' => $title,
            'user' => $user
        ]);
    }

    /**
     * Renders the contact page view.
     * 
     * Passes the page title to the contact page view for rendering. This method can be expanded
     * to handle form submissions or other interactions on the contact page.
     */
    public function contact()
    {
        $title = 'Kontakt'; // Page title

        // Render the contact page view with title
        $this->view('home/contact', [
            'title' => $title
        ]);
    }
}
