<?php

/**
 * Users controller class for managing user-related actions.
 * This class extends the Controller base class, providing functionalities
 * to list users, show individual user details, and handle user authentication
 * including login, logout, and registration processes.
 */
class Users extends Controller
{
    /**
     * Displays a list of all users.
     * 
     * Retrieves all users from the database and passes them along with the page title
     * to the 'users/index' view for rendering.
     */
    public function index()
    {
        $users = (new User)->all(); // Retrieve all users
        
        $title = 'Användare'; // Page title

        $this->view('users/index', [
            'title' => $title,
            'users' => $users
        ]); // Render the view
    }

    /**
     * Displays a list of all users in JSON format.
     * 
     * Retrieves all users from the database and sends them as a JSON response.
     * This method is useful for APIs or when responding to AJAX requests.
     */
    public function json_example()
    {
        $users = (new User)->all(); // Retrieve all users
        
        // Send the users as a JSON response
        $this->json( [
            'users' => $users
        ]); // Render the json
    }

    /**
     * Displays details for a specific user.
     * 
     * Fetches a user by ID and passes the user data along with a customized title
     * to the 'users/show' view for rendering.
     *
     * @param int $id The ID of the user to show.
     */
    public function show(int $id)
    {
        $user = (new User)->find($id); // Retrieve user by ID
        
        $title = $user['first_name'] . ' ' . $user['last_name']; // Customize page title with user's name

        $this->view('users/show', [
            'title' => $title,
            'user' => $user
        ]); // Render the view
    }

    /**
     * Handles user registration.
     * 
     * If a user is already logged in, redirects to the homepage.
     * Otherwise, attempts to create a new user with POST data and redirects on success.
     * Collects errors during user creation to pass to the 'users/create' view.
     */
    public function create()
    {
        $title = 'Registrera'; // Page title
        
        $user = (new User)->logged_in(); // Check if user is logged in
        
        $errors = []; // Initialize error collection

        if ($user) {
            $this->redirect(''); // Redirect if already logged in
        }

        if (!empty($_POST['create'])) {
            try {
                $this->verify_csrf_token($_POST['csrf_token']); // Verify the CSRF token to prevent CSRF attacks
                (new User)->create($_POST); // Attempt to create new user
                $this->redirect(''); // Redirect on success
            } catch (Exception $e) {
                $errors[] = $e->getMessage(); // Collect errors
            }
        }

        $this->view('users/create', [
            'title' => $title,
            'errors' => $errors
        ]); // Render the view with errors
    }

    /**
     * Handles user login.
     * 
     * If a user is already logged in, redirects to the homepage.
     * Otherwise, attempts to authenticate the user with POST data and redirects on success.
     * Collects errors during authentication to pass to the 'users/login' view.
     */
    public function login()
    {
        $title = 'Logga in'; // Page title
        $user = (new User)->logged_in(); // Check if user is logged in
        $errors = []; // Initialize error collection

        if ($user) {
            $this->redirect(''); // Redirect if already logged in
        }

        if (!empty($_POST['login'])) {
            try {
                $this->verify_csrf_token($_POST['csrf_token']); // Verify the CSRF token to prevent CSRF attacks
                (new User)->auth($_POST); // Attempt to authenticate user
                $this->redirect(''); // Redirect on success
            } catch (Exception $e) {
                $errors[] = $e->getMessage(); // Collect errors
            }
        }

        $this->view('users/login', [
            'title' => $title,
            'errors' => $errors
        ]); // Render the view with errors
    }

    /**
     * Handles user logout.
     * 
     * Logs the current user out and redirects to the homepage.
     */
    public function logout()
    {
        (new User)->logout(); // Log out the current user

        $this->redirect(''); // Redirect to the homepage
    }
}
