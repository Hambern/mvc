<?php

/**
 * Posts controller class for managing blog posts.
 * Inherits functionalities from the base Controller class,
 * facilitating rendering of the posts page and handling post creation.
 */
class Posts extends Controller
{
    /**
     * Displays the posts page with a list of latest blog posts.
     * 
     * If there's a POST request to create a new blog post, it attempts to create the post
     * and collect any errors. It then fetches the latest posts to display, alongside any
     * creation errors and the logged-in user's details, if available.
     */
    public function index()
    {
        $title = 'Inlägg'; // Page title for the posts page
        
        $user = (new User)->logged_in(); // Check if user is logged in and get user details
        
        $errors = []; // Initialize an array to collect potential errors during post creation

        // Attempt to create a new post if form data is submitted
        if (!empty($_POST['post'])) {
            try {
                $this->verify_csrf_token($_POST['csrf_token']); // Verify the CSRF token to prevent CSRF attacks
                $post_id = (new Post)->create($_POST); // Create a new post and get its ID
            } catch (Exception $e) {
                $errors[] = $e->getMessage(); // Collect errors if post creation fails
            }
        }

        $posts = (new Post)->latest(); // Retrieve the latest blog posts

        // Render the posts page view with collected data: title, posts, user, and any errors
        $this->view('posts/index', [
            'title' => $title,
            'posts' => $posts,
            'user' => $user,
            'errors' => $errors
        ]);
    }
}
