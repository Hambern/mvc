<?php include_once('partials/top.php') ?>

<h1><?= $title ?></h1>

<h2>Välkommen till Kvark Framework</h2>

<p>Kvark Framework är ett enkelt MVC-ramverk som alltid försöker vara så litet som möjligt.</p>

<ul>
    <li><a href="users">Användare</a></li>
    <li><a href="posts">Inlägg</a></li>
    <li><a href="contact">Kontakt</a></li>
    <?php if (!$user) : ?>
        <li><a href="users/login">Logga in</a></li>
        <li><a href="users/create">Registrera</a></li>
    <?php else : ?>
        <li><a href="users/logout">Logga ut</a></li>
    <?php endif ?>
    <li><a href="users/json_example" target="_blank">Json-exempel</a></li>
</ul>

<?php include_once('partials/bottom.php') ?>