<?php include_once('partials/top.php') ?>

<h1><?= $title ?></h1>
<p>
    <a href="users">Tillbaka</a>
</p>

<table>
    <tr>
        <th>Id</th>
        <th>Förnamn</th>
        <th>Efternamn</th>
    </tr>
    <tr>
        <td><?= $user['id'] ?></td>
        <td><?= $user['first_name'] ?></td>
        <td><?= $user['last_name'] ?></td>
    </tr>
</table>

<?php include_once('partials/bottom.php') ?>