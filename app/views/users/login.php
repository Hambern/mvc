<?php include_once('partials/top.php') ?>

<h1><?= $title ?></h1>
<p>
    <a href="index.php">Tillbaka</a>
</p>

<form method="post">
    <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token'] ?>">
    <p>
        <label for="email">Email</label>
        <input type="email" name="email" id="email">
    </p>
    <p>
        <label for="password">Lösenord</label>
        <input type="password" name="password" id="password">
    </p>
    <?php if (!empty($errors)) : ?>
        <div class="errors">
            <?php foreach ($errors as $error) : ?>
                <p><?= $error ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <input type="submit" name="login" value="Logga in">
</form>

<?php include_once('partials/bottom.php') ?>