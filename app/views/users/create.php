<?php include_once('partials/top.php') ?>

<h1><?= $title ?></h1>
<p>
    <a href="index.php">Tillbaka</a>
</p>

<form method="post">
    <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token'] ?>">
    <p>
        <label for="first_name">Förnamn</label>
        <input type="text" name="first_name" id="first_name">
    </p>
    <p>
        <label for="last_name">Efternamn</label>
        <input type="text" name="last_name" id="last_name">
    </p>
    <p>
        <label for="email">Email</label>
        <input type="text" name="email" id="email">
    </p>
    <p>
        <label for="password">Lösenord</label>
        <input type="password" name="password" id="password">
    </p>
    <p>
        <label for="password_confirm">Lösenordsbekräftelse</label>
        <input type="password" name="password_confirm" id="password_confirm">
    </p>
    <?php if (!empty($errors)) : ?>
        <div class="errors">
            <?php foreach ($errors as $error) : ?>
                <p><?= $error ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <input type="submit" name="create" value="Registrera">
</form>

<?php include_once('partials/bottom.php') ?>