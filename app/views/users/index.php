<?php include_once('partials/top.php') ?>

    <h1><?= $title ?></h1>
    <p>
        <a href="index.php">Tillbaka</a>
    </p>

    <table>
        <tr>
            <th>Id</th>
            <th>Förnamn</th>
            <th>Efternamn</th>
        </tr>
        <?php foreach ($users as $user): ?>
        <tr>
            <td>
                <a href="users/show/<?= $user['id'] ?>"><?= $user['id'] ?></a>
            </td>
            <td><?= $user['first_name'] ?></td>
            <td><?= $user['last_name'] ?></td>
        </tr>
        <?php endforeach ?>
    </table>        

<?php include_once('partials/bottom.php') ?>