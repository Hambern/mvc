<?php include_once('partials/top.php') ?>

<h1><?= $title ?></h1>
<p>
    <a href="index.php">Tillbaka</a>
</p>

<?php if ($user) : ?>
    <form method="post">
        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token'] ?>">
        <p>
            <label for="content">Inlägg</label>
            <input type="text" name="content" id="content">
        </p>
        <?php if (!empty($errors)) : ?>
            <div class="errors">
                <?php foreach ($errors as $error) : ?>
                    <p><?= $error ?></p>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <input type="submit" name="post" value="Skriv">
    </form>
    <hr>
<?php endif ?>

<table>
    <tr>
        <th>Id</th>
        <th>Förnamn</th>
        <th>Efternamn</th>
        <th>Inlägg</th>
    </tr>
    <?php foreach ($posts as $post) : ?>
        <tr>
            <td><?= $post['id'] ?></td>
            <td><?= htmlspecialchars($post['first_name']) ?></td>
            <td><?= htmlspecialchars($post['last_name']) ?></td>
            <td><?= htmlspecialchars(strip_tags($post['content'])) ?></td>
        </tr>
    <?php endforeach ?>
</table>

<?php include_once('partials/bottom.php') ?>