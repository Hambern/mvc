<?php
// Configuration file for the application
// Contains database connection parameters, base URL, session name, and table prefix and much more
return [
    'database' => [ // Change this
        'host' => 'localhost',
        'port' => 3690,
        'dbname' => 'student_chat',
        'user' => 'student',
        'password' => 'student',
    ],
    'base_url' => 'https://student.oedu.se/~mh6802/kvark/', // Change this
    'session_name' => 'kvark_framework_session',
    'table_prefix' => '',
    'session_cookie' => [
        'lifetime' => 0,
        'path' => '/~mh6802/kvark/', // Change this
        'domain' => 'student.oedu.se', // Change this
        'secure' => isset($_SERVER['HTTPS']),
        'httponly' => true,
        'samesite' => 'Strict'
    ],
    'json_options' => JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT,
    'force_https' => true,
    'debug_mode' => true,
];