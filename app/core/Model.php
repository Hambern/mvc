<?php

/**
 * Base Model class to provide common database operations.
 * This class establishes a database connection upon instantiation and offers methods
 * for performing basic database queries. All model classes within the models directory
 * should inherit from this class to leverage common database functionality.
 */
class Model
{
    /**
     * Database connection object.
     *
     * @var PDO
     */
    protected $db;

    /**
     * The name of the table associated with the model.
     *
     * @var string
     */
    public $table;

    /**
     * Constructor to establish a database connection using PDO.
     * Connection parameters are fetched from the application's configuration file.
     *
     * @throws PDOException If connection to the database fails.
     */
    public function __construct()
    {
        $config = require('app/config.php');

        $dsn = "mysql:host={$config['database']['host']};port={$config['database']['port']};dbname={$config['database']['dbname']};charset=utf8mb4";

        try {
            // Establish a database connection using PDO
            // Set the error mode to exceptions and the default fetch mode to associative arrays
            $this->db = new PDO($dsn, $config['database']['user'], $config['database']['password'], [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]);
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }

        // Set the table name with the prefix
        $this->table = $config['table_prefix'] . $this->table;
    }

    /**
     * Executes a SQL query using prepared statements.
     * Automatically determines the type of query (SELECT, INSERT, UPDATE, DELETE)
     * and returns appropriate results: last insert ID for INSERT, row count for UPDATE/DELETE,
     * and PDOStatement for SELECT.
     *
     * @param string $sql The SQL query to execute.
     * @param array $params Parameters to bind to the SQL query.
     * @return mixed The result of the query execution.
     */
    public function query(string $sql, array $params = [])
    {
        $stmt = $this->db->prepare($sql);

        foreach ($params as $key => $value) {
            $stmt->bindValue(":$key", $value, PDO::PARAM_STR);
        }

        try {
            $stmt->execute();
        } catch (PDOException $e) {
            die("Query failed: " . $e->getMessage());
        }
        
        if (preg_match('/^INSERT/i', $sql)) {
            return $this->db->lastInsertId();
        } elseif (preg_match('/^(UPDATE|DELETE)/i', $sql)) {
            return $stmt->rowCount();
        } else {
            return $stmt;
        }
    }

    /**
     * Retrieves a single record from the database by its ID.
     *
     * @param int $id The ID of the record to find.
     * @return array The fetched record.
     */
    public function find(int $id)
    {
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        
        return $this->query($sql, [
            'id' => $id
        ])->fetch();
    }

    /**
     * Retrieves all records from the associated table.
     *
     * @return array An array of all records in the table.
     */
    public function all()
    {
        $sql = "SELECT * FROM $this->table";
        
        return $this->query($sql)->fetchAll();
    }

    /**
     * Saves a new record to the database.
     *
     * @param array $data An associative array where keys are column names and values are the data to insert.
     * @return mixed The result of the query execution, typically the ID of the newly inserted record.
     */
    public function save(array $data)
    {
        $columns = implode(', ', array_keys($data));
        
        $placeholders = ':' . implode(', :', array_keys($data));
        
        $sql = "INSERT INTO $this->table ($columns) VALUES ($placeholders)";
        
        return $this->query($sql, $data);
    }

    /**
     * Updates a record in the database.
     *
     * @param int $id The ID of the record to update.
     * @param array $data An associative array where keys are column names and values are the data to update.
     * @return int The number of affected rows.
     * @throws Exception If the 'id' key is not present in the data array.
     */
    public function update(array $data)
    {
        if (empty($data['id'])) {
            throw new Exception("An 'id' key is required to update a record.");
        }

        $setClause = implode(', ', array_map(fn($column) => "$column = :$column", array_keys($data)));
        $sql = "UPDATE {$this->table} SET $setClause WHERE id = :id";

        return $this->query($sql, $data);
    }
}
