<?php

class App
{
    protected $controller = 'home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        // Load the application configuration
        $config = require('app/config.php');

        // Enforce HTTPS if enabled in the config
        $this->force_https($config['force_https']);

        // Enable or disable error reporting based on debug mode
        if ($config['debug_mode']) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        } else {
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
            error_reporting(0);
        }

        // Initialize the session using the config values
        session_name($config['session_name']);
        session_set_cookie_params($config['session_cookie']);
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        // Parse the requested URL into controller, method, and parameters
        $url = $this->parse_url();

        // Set the controller if it exists and extends the base Controller
        if (!empty($url) && class_exists($url[0]) && is_subclass_of($url[0], 'Controller')) {
            $this->controller = array_shift($url);
        }

        // Instantiate the chosen controller
        $this->controller = new $this->controller;

        // Check if the requested method is public and belongs to this controller
        if (!empty($url)) {
            $method = $url[0];
            $reflection = new ReflectionClass($this->controller);
            if (
                $reflection->hasMethod($method) &&
                $reflection->getMethod($method)->isPublic() &&
                $reflection->getMethod($method)->getDeclaringClass()->getName() === get_class($this->controller)
            ) {
                $this->method = array_shift($url);
            } else {
                http_response_code(404);
                die('404 Page Not Found');
            }
        }

        // Any remaining segments in the URL are treated as parameters
        $this->params = $url;

        // Call the determined method on the controller with the parameters
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    private function force_https($enabled)
    {
        if (
            $enabled &&
            (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on') &&
            (!isset($_SERVER['HTTP_X_FORWARDED_PROTO']) || $_SERVER['HTTP_X_FORWARDED_PROTO'] !== 'https') &&
            ($_SERVER['SERVER_PORT'] != 443)
        ) {
            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            exit;
        }
    }

    protected function parse_url()
    {
        if (isset($_GET['url'])) {
            return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
        return [];
    }
}
