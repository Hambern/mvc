<?php

class Controller
{
    private $config;

    public function __construct()
    {
        // Load the application configuration
        $this->config = require('app/config.php');

        // Rotate session if not done yet to prevent fixation attacks
        $this->rotate_session();

        // Generate a CSRF token if it doesn't exist
        if (!isset($_SESSION['csrf_token'])) {
            $this->generate_csrf_token();
        }
    }

    private function rotate_session()
    {
        if (!isset($_SESSION['initiated'])) {
            session_regenerate_id(true);
            $_SESSION['initiated'] = true;
        }
    }

    private function generate_csrf_token()
    {
        $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
    }

    public function verify_csrf_token($csrfToken)
    {
        if (
            !isset($_SESSION['csrf_token']) ||
            !hash_equals($_SESSION['csrf_token'], $csrfToken)
        ) {
            http_response_code(403);
            die('CSRF validation failed.');
        }
        session_regenerate_id(true);
        $this->generate_csrf_token();
        return true;
    }

    public function view(string $view, array $data = [], $status = 200)
    {
        extract($data, EXTR_SKIP);
        http_response_code($status);
        require_once("app/views/$view.php");
        exit;
    }

    public function json(array $data, $status = 200)
    {
        header('Content-Type: application/json');
        http_response_code($status);
        echo json_encode($data, $this->config['json_options']);
        exit;
    }

    public function redirect(string $url)
    {
        header('Location: ' . $this->config['base_url'] . $url);
        exit;
    }
}
