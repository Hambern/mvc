<?php

/**
 * Post model class for handling blog post operations.
 * Extends the base Model class to utilize common database functionality,
 * specifically tailored for operations related to the 'posts' table.
 */
class Post extends Model
{
    /**
     * Specifies the database table associated with the Post model.
     *
     * @var string
     */
    public $table = 'posts';

    /**
     * Fetches the latest posts from the database.
     * 
     * Retrieves all posts along with their associated user data, formatted by creation date in descending order.
     * The date is formatted for readability.
     *
     * @return array An array of post records, each including user details and formatted creation date.
     */
    public function latest()
    {
        $user = new User;
        
        $sql = "SELECT u.*, p.*, p.id AS id, DATE_FORMAT(p.created_at, '%e %M %H:%i') AS date
                FROM $this->table p
                JOIN $user->table u ON p.user_id = u.id
                ORDER BY p.created_at DESC";

        return $this->query($sql)->fetchAll();
    }

    /**
     * Creates a new post in the database.
     * 
     * Validates that the user is logged in and that the content is not empty before inserting the new post.
     * Throws an exception if the user is not logged in or if the content is empty.
     *
     * @param array $data The data for the new post, including content and optionally other fields.
     * @return mixed The result of the query execution, typically the ID of the newly created post.
     * @throws Exception If the user is not logged in or the content is empty.
     */
    public function create(array $data)
    {
        $user = (new User)->logged_in();

        if (!$user) {
            throw new Exception("Du är inte inloggad");
        }

        extract($data);

        if (empty($content)) {
            throw new Exception("Du måste skriva ett meddelande");
        }

        return $this->save([
            'user_id' => $user['id'],
            'content' => $content
        ]);
    }
}
