<?php

/**
 * User model class for handling user-related operations.
 * Inherits common database functionalities from the Model class, 
 * tailored specifically for 'users' table operations including authentication, 
 * login, logout, and user creation.
 */
class User extends Model
{
    /**
     * The database table associated with the User model.
     *
     * @var string
     */
    public $table = 'users';

    /**
     * Logs in a user by setting a unique token in both the database and session.
     *
     * @param int $user_id The ID of the user to log in.
     * @return bool Always returns true to indicate successful login.
     */
    public function login($user_id)
    {
        $token = bin2hex(random_bytes(16));

        $this->update([
            'id' => $user_id,
            'token' => $token,
            'token_at' => date('Y-m-d H:i:s')
        ]);

        $_SESSION['token'] = $token;

        return true;
    }

    /**
     * Logs out the currently logged-in user by clearing their session token 
     * and updating the database to remove the token.
     */
    public function logout()
    {
        $user = $this->logged_in();

        if ($user) {
            $this->update([
                'id' => $user['id'],
                'token' => '',
                'token_at' => date('Y-m-d H:i:s')
            ]);
        }

        unset($_SESSION['token']);
    }

    /**
     * Checks if a user is logged in by verifying the session token against the database.
     *
     * @return mixed The user's database record if logged in, otherwise null.
     */
    public function logged_in()
    {
        if (isset($_SESSION['token'])) {
            $token = $_SESSION['token'];

            $user = $this->query("SELECT * FROM $this->table WHERE token = :token", [
                'token' => $token
            ])->fetch();

            return $user ?: null;
        }
        return null;
    }

    /**
     * Authenticates a user based on email and password. If successful, logs the user in.
     *
     * @param array $data Data containing the email and password for authentication.
     * @return bool True on successful authentication and login, otherwise throws an exception.
     * @throws Exception If the email does not exist or the password does not match.
     */
    public function auth(array $data)
    {
        extract($data);

        $user = $this->query("SELECT * FROM $this->table WHERE email = :email", [
            'email' => $email
        ])->fetch();

        if (!$user) {
            throw new Exception("Det finns ingen användare med den e-postadressen");
        }

        if (password_verify($password, $user['password'])) {
            $this->login($user['id']);
            return true;
        } else {
            throw new Exception("Lösenordet stämde inte");
        }
    }

    /**
     * Creates a new user record in the database after validating the input data.
     *
     * @param array $data Data containing the new user's information.
     * @return bool True on successful user creation and login, otherwise throws an exception.
     * @throws Exception If any required fields are missing, if the user already exists, or if the passwords do not match.
     */
    public function create(array $data)
    {
        extract($data);

        if (empty($email) || empty($password) || empty($password_confirm)) {
            throw new Exception("Alla fält måste fyllas i");
        }

        $existingUser = $this->query("SELECT * FROM $this->table WHERE email = :email", [
            'email' => $email
        ])->fetch();

        if ($existingUser) {
            throw new Exception("Det finns redan en användare med den e-postadressen");
        }

        if ($password !== $password_confirm) {
            throw new Exception("Lösenorden stämde inte överens");
        }

        $hashed_password = password_hash($password, PASSWORD_BCRYPT);

        $user_id = $this->save([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => password_hash($password, PASSWORD_BCRYPT)
        ]);

        return $this->login($user_id);
    }
}
