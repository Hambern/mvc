<?php

/**
 * Entry point of the PHP web application.
 *
 * This file includes the main application initialization file that sets up the application environment,
 * autoloads classes, and performs any necessary preliminary configurations.
 * It then instantiates the App class which serves as the central point for handling the incoming request,
 * routing it to the appropriate controller and method based on the request URL.
 */

// Include the main application initialization file that sets up the application environment,
// autoloads classes, and performs any necessary preliminary configurations.
require_once 'app/init.php';

// Instantiate the App class which serves as the central point for handling the incoming request,
// routing it to the appropriate controller and method based on the request URL.
$app = new App();
