# Kvark Framework

Kvark är ett litet och enkelt MVC-ramverk inspirerat av [Codecourses YouTube-serie](https://www.youtube.com/watch?v=OsCTzGASImQ&list=PLfdtiltiRHWGXVHXX09fxXDi-DqInchFD). Syftet med ramverket är att ge en pedagogisk grund för att förstå och experimentera med MVC-arkitekturen. Tanken är att den ska göra så pass lite att den är enkel att förstå, studera, anpassa och använda till vilket projekt som helst utan att man ska behöva sätta sig in komplicerade paket.

## Innehållsförteckning
- [Introduktion](#introduktion)
- [Applikationsflöde](#applikationsflöde)
- [Filstruktur](#filstruktur)
- [Installation](#installation)
- [Hämta uppdateringar](#hämta-uppdateringar)
- [Routing](#routing)
- [Användande](#användande)
- [Säkerhet](#säkerhet)
- [Bidra](#bidra)
- [Licens](#licens)

## Introduktion
Kvark Framework är ett PHP-ramverk som använder sig av en enkel MVC-struktur. MVC (Model-View-Controller) är ett designmönster som delar upp en applikation i tre huvudsakliga delar:
- **Modeller (Models):** Hanterar data, affärslogik och kommunikation med databasen.
- **Vyer (Views):** Ansvarar för presentationen, dvs. hur informationen visas för användaren.
- **Controllers:** Verkar som en mellanhand som tar emot användarförfrågningar, hämtar och lagrar data via modeller och skickar den vidare till vyerna.

Genom att arbeta med Kvark får du en praktisk inblick i hur dessa komponenter samverkar för att skapa en strukturerad och skalbar webbapplikation. När du förstår Kvark har du lättare att därefter lära dig större ramverk som [Laravel](https://laravel.com).

## Applikationsflöde
Applikationen startar från filen `index.php`, som fungerar som ingångspunkt. Här sker följande:
- **Laddar `init.php`:**  
  Denna fil konfigurerar miljön (t.ex. inställningar och autoladdning av klasser) och förbereder applikationen för att köras.
- **Instansierar `App`-klassen:**  
  `App`-klassen ansvarar för routing, vilket innebär att den tolkar URL-anrop och bestämmer vilken controller som ska hantera varje förfrågan.

### Controllers
Controllers ligger i mappen `app/controllers`. Dessa filer:
- Tar emot användarinmatningar (t.ex. klick och formulärdata).
- Hämtar nödvändig data från modeller.
- Väljer vilken vy som ska användas för att presentera data till användaren.

Genom att separera logiken i olika controllers blir koden enklare att förstå och underhålla

### Models
Modellerna finns i `app/models` och representerar applikationens data samt hanterar interaktionen med databasen. Genom att använda modeller isoleras datahanteringen från affärslogiken, vilket bidrar till en renare kodstruktur.

### Views
Vyerna, som ligger i `app/views`, ansvarar för att generera HTML-utdata baserat på den data som skickas från controllers. Om du däremot bygger ett API, där data utbyts i JSON-format, kan du helt bortse från denna mapp.

### Core
I `app/core` finns de grundläggande klasserna som utgör ramverkets ryggrad. Dessa behöver du sällan ändra i:
- **App.php:** Hanterar routing och applikationens initialisering.
- **Controller.php:** Är basen för alla controllers. Här definieras gemensam funktionalitet som alla controllers kan ärva.
- **Model.php:** Är basen för alla modeller och innehåller logik för databasinteraktion och andra gemensamma metoder.

### Övrigt innehåll
- **Assets:**  
  Mappen `assets` innehåller statiska filer, såsom CSS och JavaScript, som används för att styla och interagera med din applikation.
- **Partials:**  
  I mappen `partials` finns återanvändbara HTML-snuttar (t.ex. sidhuvuden, menyer och sidfötter) som hjälper dig att undvika kodupprepning och göra globala ändringar enkelt.

## Filstruktur
Nedan visas en översikt över projektets struktur, vilket hjälper dig att snabbt förstå var de olika delarna av ramverket finns:

~~~plaintext
.
├── app
│   ├── config.example.php
│   ├── controllers
│   │   ├── home.php
│   │   ├── posts.php
│   │   └── users.php
│   ├── core
│   │   ├── App.php
│   │   ├── Controller.php
│   │   └── Model.php
│   ├── init.php
│   ├── models
│   │   ├── Post.php
│   │   └── User.php
│   └── views
│       ├── home
│       │   ├── contact.php
│       │   └── index.php
│       ├── posts
│       │   └── index.php
│       └── users
│           ├── create.php
│           ├── index.php
│           ├── login.php
│           └── show.php
├── assets
│   └── style.css
├── index.php
├── partials
│   ├── bottom.php
│   └── top.php
└── README.md
~~~

## Installation
Följ dessa steg för att komma igång med Kvark:
1. **Forka repositoryt:**  
   Besök projektets sida på GitLab och klicka på "Fork". Detta skapar en egen kopia av projektet under ditt konto, vilket gör att du kan experimentera fritt utan att påverka originalkoden.
2. **Klona din fork:**  
   När du har forkat repositoryt, klona din fork med följande kommando:
   ```bash
   git clone git@gitlab.com:<ditt-användarnamn>/kvark.git
   ```
3. **Navigera till projektets rotkatalog:**
   ```bash
   cd kvark
   ```
4. **Konfigurera databasen:**  
   Kopiera filen `app/config.example.php` till `app/config.php` och ändra de uppgifter som är utmärkta med `Change this`. Detta steg är avgörande för att applikationen ska kunna kommunicera med databasen bland annat.
5. **Starta applikationen:**  
   Öppna `index.php` i din webbläsare. Om allt är korrekt konfigurerat bör du nu se applikationen i drift.

## Hämta uppdateringar
Efter att du har forkat projektet kommer nya ändringar i originalrepositoryt (upstream) inte automatiskt dyka upp i din fork. Gör så här för att hämta och slå ihop uppdateringar:

### Lägg till originalprojektet som en “upstream”-remote
~~~bash
git remote add upstream git@gitlab.com:hambern/kvark.git
~~~

Hämta sedan nya ändringar från `upstream`
~~~bash
git fetch upstream
~~~

Nu har du lokalt laddat ner alla nya commits som finns i originalprojektets grenar. Slå nu samman dessa ändringar med din egna gren

~~~bash
git checkout master
git merge upstream/master
~~~

Har du konflikter, får du lösa dem manuellt innan du gör en ny commit. Skicka därefter upp dina ändringar till din fork

~~~bash
git push origin master
~~~

Nu ligger din fork uppdaterad på GitLab med de senaste ändringarna från originalprojektet.

## Routing
Kvark använder sig av en enkel routing-mekanism. När du anropar en URL på din sida, till exempel `minhemsida.se/posts`, söker Kvark efter en controller som heter `Posts` och anropar dess `index`-metod. Om ingen sådan controller finns, letar den istället efter en metod med det namnet i din `Home`-controller och anropar den.

Om du anropar en URL som `minhemsida.se/posts/show/1` kommer Kvark att anropa `Posts`-controllerns `show`-metod med `1` som `$id`-variabel. Detta innebär att du kan skapa in en ny sida genom att bara lägga till en ny metod i din controller.

Detta innebär följande:
- `minhemsida.se/users` anropar `Users`-controllerns `index`-metod
- `minhemsida.se/users/show/1` anropar `Users`-controllerns `show`-metod med `$id`-variabeln satt till `1`
- `minhemsida.se/contact` anropar `Home`-controllerns `contact`-metod
- `minhemsida.se/` anropar `Home`-controllerns `index`-metod

## Användande
Detta avsnitt innehåller praktiska tips och riktlinjer för hur du kan använda och anpassa Kvark, oavsett om du bygger en traditionell webbapplikation eller ett API.

- **Studera koden:**  
  Börja med att gå igenom `App`-klassen för att se hur routing fungerar. Titta sedan på hur controllers hanterar användarinmatningar och hur modeller hämtar och bearbetar data. Genom att följa datans resa från URL till vy får du en helhetsförståelse för MVC-mönstret.

- **Skapa egna controllers:**  
  Använd exempel från mappen `app/controllers` som mall för att bygga nya controllers. Detta är ett bra sätt att lära sig genom att göra – försök att implementera nya funktioner och se hur de integreras med resten av systemet.

- **Utveckla nya vyer:**  
  Anpassa eller skapa egna vyer i `app/views` för att presentera data på det sätt du vill. Om du bygger ett API kan du hoppa över detta steg, eftersom du istället kommer att skicka data i JSON-format.

- **Experimentera med modellerna:**  
  Modellerna i `app/models` är en utmärkt plats att börja om du vill förstå hur data lagras och hanteras. Försök att lägga till nya metoder eller modifiera befintliga för att se hur de påverkar applikationens funktion. En bra generell princip är att alla tabeller bör ha en modell och att specifika databasfrågor till den tabellen skapas i form av en metod som kan användas i controllern.

- **Rensa posts-relaterade mappar:**  
  Om ditt projekt inte hanterar inlägg (posts) rekommenderas det att du tar bort följande mappar och filer:
  - `app/controllers/posts.php`
  - `app/models/Post.php`
  - `app/views/posts`  
  Detta ger dig en renare start, så att du kan bygga exakt de funktioner som ditt projekt behöver.

- **Tips för API-utveckling:**  
  Om du vill bygga ett API där svaren ska skickas som JSON istället för HTML, kan du byta ut metoden `view` mot en metod som returnerar JSON. Genom att ändra din controller från:
  ```php
  public function index()
  {
      $users = (new User)->all(); // Hämta alla användare
      $title = 'Användare'; // Sidans titel

      $this->view('users/index', [
          'title' => $title,
          'users' => $users
      ]);
  }
  ```
  till:
  ```php
  public function index()
  {
      $users = (new User)->all(); // Hämta alla användare

      $this->json([
          'users' => $users
      ]);
  }
  ```
  får du ett JSON-svar som är perfekt anpassat för API:er. Detta visar hur flexibel arkitekturen är och hur du enkelt kan byta mellan olika typer av svar beroende på projektets behov.

- **Lär dig av feedback:**  
  Var inte rädd för att experimentera och göra ändringar i koden. Dokumentera vad du ändrar och varför, samt lyssna på feedback från användare och medutvecklare. Detta är en viktig del av lärprocessen och hjälper dig att utvecklas som programmerare.

## Säkerhet  
Kvark har inbyggda säkerhetsåtgärder för att skydda applikationen:  

- **HTTPS-omdirigering** – Tvingar HTTPS om aktiverat i `config.php`.  
- **CSRF-skydd** – Genererar och validerar en CSRF-token vid formulärinmatning.  
- **Säkra sessioner** – Använder säkra cookies (`httponly`, `secure`, `samesite`).  
- **SQL Injection-skydd** – Alla databasförfrågningar använder **prepared statements**.  
- **XSS-skydd** – HTML-escapar användarinmatning (`htmlspecialchars()`).  
- **Säker routing** – Endast befintliga controllers och publika metoder kan anropas.  

Dessa åtgärder minskar risken för attacker och skyddar både system och användare.

## Bidra
Vill du bidra till att förbättra Kvark? Det går till så här:
- Skicka in en pull request med en tydlig beskrivning av dina ändringar.
- Följ projektets befintliga kodstandard och struktur.
- Om du hittar buggar eller har förslag på förbättringar, öppna ett issue med en detaljerad beskrivning.

## Licens
Detta projekt är licensierat under Creative Commons-licensen, vilket innebär att du fritt kan använda, modifiera och distribuera koden.